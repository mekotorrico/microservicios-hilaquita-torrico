package vistas;

import java.awt.BorderLayout;
import java.io.IOException;
import java.net.URISyntaxException;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmTienda extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmTienda frame = new FrmTienda();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmTienda() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTienda = new JLabel("TIENDA");
		lblTienda.setBounds(12, 12, 70, 15);
		contentPane.add(lblTienda);
		
		JButton btnNewButton = new JButton("Usuarios");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(java.awt.Desktop.isDesktopSupported()) {
					java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
					
					if(desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
						try {
						java.net.URI url1 = new java.net.URI("https://www.youtube.com/");
						desktop.browse(url1);
						}catch(URISyntaxException | IOException ex) {}
					}
				}
			}
		});
		btnNewButton.setBounds(12, 56, 117, 25);
		contentPane.add(btnNewButton);
		
		JButton btnVentas = new JButton("Ventas");
		btnVentas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(java.awt.Desktop.isDesktopSupported()) {
					java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
					
					if(desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
						try {
						java.net.URI url1 = new java.net.URI("https://www.youtube.com/");
						desktop.browse(url1);
						}catch(URISyntaxException | IOException ex) {}
					}
				}
				
			}
		});
		btnVentas.setBounds(12, 93, 117, 25);
		contentPane.add(btnVentas);
		
		JButton btnInventarios = new JButton("Inventarios");
		btnInventarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(java.awt.Desktop.isDesktopSupported()) {
					java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
					
					if(desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
						try {
						java.net.URI url1 = new java.net.URI("https://www.youtube.com/");
						desktop.browse(url1);
						}catch(URISyntaxException | IOException ex) {}
					}
				}
				
			}
		});
		btnInventarios.setBounds(12, 130, 117, 25);
		contentPane.add(btnInventarios);
	}
}

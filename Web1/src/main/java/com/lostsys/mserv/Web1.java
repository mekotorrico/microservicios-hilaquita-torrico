package com.lostsys.mserv;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@SpringBootApplication
@Controller
public class Web1 {
	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	private LoadBalancerClient loadBalancer;
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;	
	
	private static ArrayList<String> kafkamsgs=new ArrayList<String>();
	
	public static void main(String[] args) {
		SpringApplication.run(Web1.class, args);
	}
	
	@RequestMapping("/")
	@ResponseBody
	public String home( HttpServletRequest request) {
		StringBuilder sb=new StringBuilder();
		
		sb.append("<h1>	<form class=\"form\" role=\"form\" autocomplete=\"off\">\n"
				+ "		<h2> Gestión de Usuarios</h2>\n"
				+ "	<div class=\"form-group row\">\n"
				+ "		<label class=\"col-lg-3 col-form-label form-control-label\">Nombres</label>\n"
				+ "		<div class=\"col-lg-9\">\n"
				+ "			<input class=\"form-control\" type=\"text\" >\n"
				+ "		</div>\n"
				+ "	</div>\n"
				+ "	<div class=\"form-group row\">\n"
				+ "		<label class=\"col-lg-3 col-form-label form-control-label\">Apellidos</label>\n"
				+ "		<div class=\"col-lg-9\">\n"
				+ "			<input class=\"form-control\" type=\"text\" >\n"
				+ "		</div>\n"
				+ "	</div>\n"
				+ "	<div class=\"form-group row\">\n"
				+ "		<label class=\"col-lg-3 col-form-label form-control-label\">Correo</label>\n"
				+ "		<div class=\"col-lg-9\">\n"
				+ "			<input class=\"form-control\" type=\"email\">\n"
				+ "		</div>\n"
				+ "	</div>\n"
				+ "	<div class=\"form-group row\">\n"
				+ "		<label class=\"col-lg-3 col-form-label form-control-label\">Contraseña</label>\n"
				+ "		<div class=\"col-lg-9\">\n"
				+ "			<input class=\"form-control\" type=\"password\">\n"
				+ "		</div>\n"
				+ "	</div>\n"
				+ "	<div class=\"form-group row\">\n"
				+ "		<div class=\"col-lg-12 text-center\">\n"
				+ "			<input type=\"reset\" class=\"btn btn-secondary\" value=\"Cancelar\">\n"
				+ "			<input type=\"button\" class=\"btn btn-primary\" value=\"Guardar\">\n"
				+ "		</div>\n"
				+ "	</div>\n"
				+ "</form></h1>");
		
		/* Service Discovery */
		if (loadBalancer.choose("Microservicio1")!=null) sb.append( "<p>load balancer: "+loadBalancer.choose("Microservicio1").getInstanceId()+"</p>" );
		if (discoveryClient.getInstances("Microservicio1")!=null) sb.append( "<p>instances: "+discoveryClient.getInstances("Microservicio1").size()+"</p>" );
		
		/* Events */
		if ( request.getParameter("kafkamsg")!=null ) {
			ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send("microservicio1",  request.getParameter("kafkamsg"));
	    	
			future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
				public void onSuccess(SendResult<String, String> result) { System.out.println( result ); }
				public void onFailure(Throwable ex) { ex.printStackTrace(); }
				});   
			}
		sb.append( "<p>msgs: "+kafkamsgs+"</p>" );
		sb.append( "<p><form method='post' action='/'><input name='kafkamsg' /><input type='submit' value='Enviar'/></form></p>" );
		
		return sb.toString();
	}
	@KafkaListener(topics = "microservicio1", groupId = "grupo1")
	public void listenTopic2(String message) {
		kafkamsgs.add( message );
		}
}

# Microservicios-Hilaquita-Torrico
# Engineering ledger 
# Se enfoco el proyecto en los microservicios para una tienda.
# Se utilizó el IDE Eclipse para el desarrollo de los microservicios en java.
# Se realizó la implementación de aplicacion Spring Boot
#   Se crean tres ficheros básicos: 
#      1º Fichero de configuración Maven: Es la herramienta para soporte a dependencias y compilación, se llama 'pom.xml'.
#      2º Fichero de configuración'application.properties': Para configurar el nombre de la aplicación (microservicio) y el puerto
#      3º Clase principal de la aplicación: Código.
# Ejecutar la aplicación en la terminal con el siguiente comando: 'mvn spring-boot:run'
# Integrar Spring con Hashicorp Consul.
# Se crea el contenedor de Consul en Docker.
# Se añaden dependencias en el fichero 'pom' de maven.
# Se configura el fichero 'application.properties' para configurar la conexión a Consul.
# Se realiza el código para el uso de Consul.
# Instalación de Apache Kafka e integración de Spring Framework.
# Se configura el fichero 'application.properties' para configurar la conexión al Kafka.
# Se declara topics para la comunicación del microservicio y sus instancias.
# Se realizó el código para la comunicación con Kafka.
# Se realizarón los mismos pasos para lo demás microservicios.

package com.lostsys.mserv2;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lostsys.mserv2.Web2;

@SpringBootApplication
@Controller

public class Web2 {
	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	private LoadBalancerClient loadBalancer;
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;	
	
	private static ArrayList<String> kafkamsgs=new ArrayList<String>();
	
	public static void main(String[] args) {
		SpringApplication.run(Web2.class, args);
	}
	
	@RequestMapping("/")
	@ResponseBody
	public String home( HttpServletRequest request) {
		StringBuilder sb=new StringBuilder();
		
		sb.append("<h1>	<form class=\"form\" role=\"form\" autocomplete=\"off\">\n"
				+ "		<h2> Gestión de Inventarios</h2>\n"
				+ "	<div class=\"form-group row\">\n"
				+ "		<label class=\"col-lg-3 col-form-label form-control-label\">Producto</label>\n"
				+ "		<div class=\"col-lg-9\">\n"
				+ "			<input class=\"form-control\" type=\"text\" >\n"
				+ "		</div>\n"
				+ "	</div>\n"
				+ "	<div class=\"form-group row\">\n"
				+ "		<label class=\"col-lg-3 col-form-label form-control-label\">Cantidad</label>\n"
				+ "		<div class=\"col-lg-9\">\n"
				+ "			<input class=\"form-control\" type=\"number\" >\n"
				+ "		</div>\n"
				+ "	</div><br>\n"
				+ "	<div class=\"form-group row\">\n"
				+ "		<div class=\"col-lg-12 text-center\">\n"
				+ "			<input type=\"button\" class=\"btn btn-primary\" value=\"Guardar\">\n"
				+ "		</div>\n"
				+ "	</div>\n"
				+ "</form>\n"
				+ "	\n"
				+ "	\n"
				+ "	<table class=\"default\" border=\"1\">\n"
				+ "    	<tr>\n"
				+ "    		<th>Nº</th>\n"
				+ "    		<th>Fecha</th>\n"
				+ "    		<th>Poleras</th>\n"
				+ "    		<th>Busos</th>\n"
				+ "    		<th>Chamarras</th>\n"
				+ "    		<th>Gorras</th>\n"
				+ "    		<th>Zapatillas</th>\n"
				+ "  		</tr>\n"
				+ "  		<tr>\n"
				+ "    		<td>1</td>\n"
				+ "    		<td>10/01/2020</td>\n"
				+ "    		<td>10000</td>\n"
				+ "    		<td>18600</td>\n"
				+ "    		<td>70500</td>\n"
				+ "    		<td>10250</td>\n"
				+ "    		<td>9000</td>\n"
				+ "  		</tr>\n"
				+ "  		<tr>\n"
				+ "    		<td>2</td>\n"
				+ "    		<td>10/01/2020</td>\n"
				+ "    		<td>10000</td>\n"
				+ "    		<td>18600</td>\n"
				+ "    		<td>70500</td>\n"
				+ "    		<td>10250</td>\n"
				+ "    		<td>9000</td>\n"
				+ " 		</tr>\n"
				+ "  		<tr>\n"
				+ "    		<td>3</td>\n"
				+ "    		<td>10/01/2020</td>\n"
				+ "    		<td>10000</td>\n"
				+ "    		<td>18600</td>\n"
				+ "    		<td>70500</td>\n"
				+ "    		<td>10250</td>\n"
				+ "    		<td>9000</td>\n"
				+ " 		</tr>\n"
				+ "	</table></h1>");
		
		/* Service Discovery */
		if (loadBalancer.choose("Microservicio2")!=null) sb.append( "<p>load balancer: "+loadBalancer.choose("Microservicio2").getInstanceId()+"</p>" );
		if (discoveryClient.getInstances("Microservicio2")!=null) sb.append( "<p>instances: "+discoveryClient.getInstances("Microservicio2").size()+"</p>" );
		
		/* Events */
		if ( request.getParameter("kafkamsg")!=null ) {
			ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send("microservicio2",  request.getParameter("kafkamsg"));
	    	
			future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
				public void onSuccess(SendResult<String, String> result) { System.out.println( result ); }
				public void onFailure(Throwable ex) { ex.printStackTrace(); }
				});   
			}
		sb.append( "<p>msgs: "+kafkamsgs+"</p>" );
		sb.append( "<p><form method='post' action='/'><input name='kafkamsg' /><input type='submit' value='Enviar'/></form></p>" );
		
		return sb.toString();
	}
	@KafkaListener(topics = "microservicio2", groupId = "grupo2")
	public void listenTopic2(String message) {
		kafkamsgs.add( message );
		}
}
